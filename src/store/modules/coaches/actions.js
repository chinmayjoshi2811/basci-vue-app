export default {
  registerCoach(context, data) {
    const { firstName, lastName, rate, areas } = data;
    const coachData = {
      id: context.rootGetters.userId,
      firstName,
      lastName,
      hourlyRate: rate,
      areas,
    };
    context.commit('registerCoach', coachData);
  },
};
