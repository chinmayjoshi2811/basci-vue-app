export default {
  requests(state, getters, rootState, rootGetters) {
    const coachId = rootGetters.userId;
    return state.requests.filter((x) => x.coachId === coachId);
  },
  hasRequests(state, getters) {
    const reqs = getters.requests;
    return reqs && reqs.length > 0;
  },
};
